from flask import Flask
from flask_restful import Resource, Api

app = Flask(__name__)
api = Api(app)

# print(app)
# print(api)


#  Code our flask api

class Product(Resource):
    def get(self):
        return {
            "products":["beer infused muffin", "double chocolate", "duffin", "rasin muffin"]
        }

class Product_price(Resource):
    def get(self):
        return{
            "price":[12,10,17,8]
        }


#  Expose this in the api

api.add_resource(Product,'/')
api.add_resource(Product_price, '/price')

if __name__ == "__main__":
    print("This is main")
    app.run("0.0.0.0", port=80, debug=True)
